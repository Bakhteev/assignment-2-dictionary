%define pointer 0

%macro colon 2
  %2: 
  dq pointer
  %define pointer %2
  db %1, 0
%endmacro