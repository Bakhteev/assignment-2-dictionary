section .data

char: db ''
numbers: db "0123456789"
minus: db "-"
newLine: db 10, 0

section .text

global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

exit:
		mov rax, 60
   	syscall

string_length:
    xor rax, rax
    .loop:
			cmp byte [rdi+rax],0
			je .end
			inc rax
			jmp .loop

	.end:
		ret

print_string:
		call string_length
		mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, 1
   	syscall
    ret

print_error:
		call string_length
		mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, 2
   	syscall
    ret

print_char:
	mov [char], rdi
	mov rsi, char
	mov rax, 1
	mov rdi, 1
	mov rdx, 1
	syscall
  ret

print_newline:
	mov rax, 1
	mov rdi, 1
	mov rsi, newLine
	mov rdx, 1
	syscall
  ret

print_uint:
	xor rcx, rcx
	mov rax, rdi
	mov rbx, 10
	.loop:
		xor rdx, rdx
		inc rcx
		div rbx
		push rdx
		cmp rax, 0
		jnz .loop
		xor rbx, rbx
	.loop_print:
		dec rcx
		pop rdx
		push rcx
		mov rax, 1
		mov rdi, 1
		lea rsi, [numbers+rdx]
		mov rdx, 1
		syscall
		pop rcx
		cmp rcx, 0
		jnz .loop_print
    ret

print_int:
	mov rax, rdi
	cmp rax, 0
	js .neg
	call print_uint
  ret
	.neg:
		mov rbx, rdi
		mov rax, 1
		mov rdi, 1
		mov rsi, minus
		mov rdx, 1
		syscall
		mov rax, rbx
		neg rax
		mov rdi, rax
		call print_uint
	ret


string_equals:
	push rdi
	push rsi
	call string_length
	push rax
	mov rdi, rsi
	call string_length
	pop rdx
	pop rsi
	pop rdi
	cmp rax, rdx
	jne .false
	mov r8, rax 
	cmp r8, 0
	je .true
	xor rcx, rcx
	.loop:
		mov al, [rdi + rcx]
		mov dl, [rsi + rcx]
		cmp al, dl
 		jne .false
		inc rcx
		cmp rcx, r8
		je .true
		jmp .loop
	.true:
		mov rax, 1
		ret
	.false:
		mov rax, 0
    ret

read_char:
	mov rax, 0
	mov rdi, 0
	mov rsi, char
	mov rdx, 1
	syscall
	cmp al, 0
	je .end
	mov al, [char]
  ret 
	.end:
		xor rax, rax
		ret
 
read_word:
	push rsi
	xor rcx, rcx
	push rcx
	.check_spaces:
		push rdi
		call read_char
		cmp rax, 0
		je .null
		pop rdi
		mov al, [rsi]
		cmp al, 10
		je .check_spaces
		cmp al, 32
		je .check_spaces
		cmp al, 9
		je .check_spaces
		mov byte [rdi], al
	.loop:
		pop rcx
		pop rax
		cmp rax, rcx
		je .err
		push rax
		push rcx
		push rdi
		call read_char
		mov rdx, rax
		pop rdi
		pop rcx
		mov al, [rsi]
		inc rcx
		mov byte [rdi+rcx], al
		push rcx
		mov rax, rdx
		cmp al, 10
		je .end
		cmp al, 32
		je .end
		cmp al, 9
		je .end
		cmp rax, 0
		jnz .loop
	.end:
		pop rcx 
		mov byte [rdi+rcx], 0
		pop rdx
		mov rdx, rcx
		mov rax, rdi
		ret
	.null:
		pop rax
		pop rax
		pop rax
		xor rax, rax
		xor rdx, rdx
    ret
	.err:
		xor rax, rax
		xor rdx, rdx
    ret

parse_uint:
  xor rax, rax
  xor rcx, rcx
  mov al, [rdi + rcx]
  cmp al, 48
  jl .not_number
  cmp al, 57
  jg .not_number
  sub rax, 48
  push rax
  .loop:
    inc rcx
    mov al, [rdi + rcx]
    cmp rax, 48 	; ascii 0
    jl .end_of_number
    cmp rax, 57 	; ascii 9	
    jg .end_of_number
    sub rax, 48
    push rax
    jmp .loop
  .not_number:
    xor rax, rax
		xor rdx, rdx
		ret
  .end_of_number:
    xor rdi, rdi
		mov rdx, rcx
  .end_loop:
		mov rax, 1
		mov rsi, rdx
		sub rsi, rcx
	.sqr:
		cmp rsi, 0
		je .next
		imul rax, 10
		dec rsi
		jmp .sqr
	.next:
		pop rsi
		imul rax, rsi
		add rdi, rax
		dec rcx
		test rcx, rcx
		jne .end_loop
	mov rax, rdi
  ret

parse_int:
  xor rax, rax
  mov al, [rdi]
	cmp al, [minus]
	je .negative
	cmp al, 48 	; ascii 0
	jl .NaN
	cmp al, 57 	; ascii 9
	jg .NaN
	sub rax, 48
	call parse_uint
	ret
	.negative:
		inc rdi
		call parse_uint
		inc rdx
		neg rax
		ret
	.NaN:
		xor rax, rax
		xor rdx, rdx
		ret


string_copy:
	call string_length
	cmp rax, rdx
	jg .err_size
	mov rcx, rax
	xor rdx, rdx
	.loop:
		mov rax, [rdi+rdx]
  	mov qword [rsi+rdx], rax
		xor rax, rax
		cmp cl, 8
		jg .mov_cycle
		jmp .next_copy
	.mov_cycle:
		sub rcx, 8
		add dl, 8
		jmp .loop
	.next_copy:
		mov rdi, rsi
		call string_length
		ret
	.err_size:
		xor rax, rax
		ret

