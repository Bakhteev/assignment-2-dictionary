section .bss
buf: times 255 db 0

section .rodata
out_of_bound: db "string is too long", 0
not_found: db "string not found", 0

section .data
%include "words.inc"

section .text
global _start
%include "lib.inc"
_start:
  mov rdi, buf
  mov rsi, 256
  push rdi
  call read_word
  pop rdi
  cmp rax, 0
  je .out_of_bound
  mov rsi, pointer
  call find_word
  cmp rax, 0
  je .not_found
  add rax, 8
  push rax
  mov rdi, rax
  call string_length
  pop rdi
  add rdi, rax
  inc rdi
  call print_string
  call print_newline
  xor rdi, rdi
  call exit
  .not_found:
    mov rdi, not_found
    call print_error
    call print_newline
    mov rdi, 1
    call exit      
  .out_of_bound:
    mov rdi, out_of_bound
    call print_error
    call print_newline
    mov rdi, 1
    call exit